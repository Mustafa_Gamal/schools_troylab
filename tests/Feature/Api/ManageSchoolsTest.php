<?php

namespace Tests\Feature\Api;

use App\Models\School;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageSchoolsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_can_list_schools()
    {
        $school = School::factory()->create();

        $response = $this->getJson('/api/schools');

        $response->assertStatus(200)->assertJsonStructure([
            'message',
            'code',
            'data'
        ])->assertSee($school->name);
    }
}
