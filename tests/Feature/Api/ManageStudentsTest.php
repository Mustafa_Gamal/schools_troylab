<?php

namespace Tests\Feature\Api;

use App\Models\School;
use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ManageStudentsTest extends TestCase
{
    use RefreshDatabase;

    private const endPoint = '/api/students/';

    private $student;

    public function setUp(): void
    {
        parent::setUp();
        $this->student = Student::factory()->create();
    }

    /** @test */
    public function a_user_can_list_students()
    {
        $this->signIn(null, true);

        $response = $this->getJson(ManageStudentsTest::endPoint);

        $response->assertStatus(200)->assertJsonStructure([
            'message',
            'code',
            'data',
        ])->assertSee($this->student->name);
    }

    /** @test */
    public function a_user_can_create_studnet()
    {

        $this->signIn(null, true);

        $student = Student::factory()->make([
            'school_id' => School::factory()->create()->id,
        ]);

        $response = $this->postJson(ManageStudentsTest::endPoint, $student->toArray());

        $this->assertDatabaseHas("students", $student->toArray());

        $response->assertStatus(200);
    }

    /** @test */
    public function a_user_can_edit_student()
    {
        $this->signIn(null, true);

        $data = $this->student->toArray();

        $data['name'] = "test user";

        $this->putJson(ManageStudentsTest::endPoint . $this->student->id, $data)->assertStatus(200);

        $this->assertDatabaseHas('students', ["name" => "test user"]);
    }

    /** @test */
    public function a_user_can_show_student()
    {
        $this->signIn(null, true);

        $response = $this->getJson(ManageStudentsTest::endPoint . $this->student->id)->assertSee([
            'name' => $this->student->name,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function a_user_can_delete_student()
    {
        $this->signIn(null, true);

        $this->deleteJson(ManageStudentsTest::endPoint . $this->student->id)
            ->assertStatus(200);

        $this->assertSoftDeleted('students', ["id" => $this->student->id]);
    }
}
