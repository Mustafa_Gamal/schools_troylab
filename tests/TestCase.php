<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn($user = null, $passport = false)
    {
        $user = $user ?? User::factory()->create();

        if($passport){
            Passport::actingAs($user);
        }else{
            /** @var \Illuminate\Contracts\Auth\Authenticatable $user */
            $this->actingAs($user);
        }

        return $user;

    }
}
