<?php

namespace Tests\Unit;

use App\Models\School;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SchoolTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function has_students()
    {
        $school = School::factory()->create();

        $this->assertInstanceOf(Collection::class, $school->students);
    }
}
