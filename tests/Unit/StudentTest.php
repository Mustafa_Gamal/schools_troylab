<?php

namespace Tests\Unit;

use App\Models\School;
use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function belongs_to_school()
    {
        $student = Student::factory()->create();

        $this->assertInstanceOf(School::class, $student->school);
    }
}
