<?php

namespace Tests\Setup;

use App\Models\School;
use App\Models\Student;

class SchoolFactory
{
    protected $studentsCount = 0;


    public function withStudents($count)
    {
        $this->studentsCount = $count;

        return $this;
    }

    public function create()
    {
        $school = School::factory()->create();

        Student::factory()->count($this->studentsCount)->create([
            "school_id" => $school->id
        ]);

        return $school;
    }
}
