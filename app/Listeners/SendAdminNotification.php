<?php

namespace App\Listeners;

use App\Events\NotifyAdminEvent;
use App\Models\User;
use App\Notifications\AdminNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendAdminNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotifyAdminEvent  $event
     * @return void
     */
    public function handle(NotifyAdminEvent $event)
    {
        //admin user
        User::first()->notify(new AdminNotification($event->student));
    }
}
