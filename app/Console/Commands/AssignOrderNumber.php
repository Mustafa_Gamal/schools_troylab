<?php

namespace App\Console\Commands;

use App\Events\NotifyAdminEvent;
use App\Models\Student;
use Illuminate\Console\Command;

class AssignOrderNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'students:assignOrder {school}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command to assign order number and fire an event on finish';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $schoolId = $this->argument('school');

        //get latest order of school
        $latestStudent = Student::where("school_id", $schoolId)->where("order", "!=" ,null)->orderBy("id", "desc")->first();
        //get first student in school without order
        $student = Student::where("order", null)->where("school_id", $schoolId)->first();
        if(!$student){
           return $this->error("Student or School not found");
        }
        //update
        $student->update([
            "order" => $latestStudent ? $latestStudent->order + 1 :  1
        ]);

        event(new NotifyAdminEvent($student));

        $this->info("Assign order to student and notify the admin successfully");
    }
}
