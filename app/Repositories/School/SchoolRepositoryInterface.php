<?php

namespace App\Repositories\School;

use Illuminate\Http\Request;

interface SchoolRepositoryInterface
{
    public function all(Request $request, $paginate = false);
}
