<?php

namespace App\Repositories\School;

use App\Models\School;
use Illuminate\Http\Request;

class SchoolRepository implements SchoolRepositoryInterface
{

    private $model;

    public function __construct(School $model)
    {
        $this->model = $model;
    }

    public function all(Request $request, $paginate = false)
    {
        $q = $this->model->query();

        if($request->has("order")){
            $q->orderBy('created_at', $request->order);
        }

        return $paginate ? $q->paginate()->withQueryString() : $q->get();
    }

}


