<?php

namespace App\Repositories\Student;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentRepository implements StudentRepositoryInterface
{

    private $model;

    public function __construct(Student $model)
    {
        $this->model = $model;
    }

    public function all(Request $request, $paginate = false)
    {
        $q = $this->model->query();

        if($request->has("school_id")){
            $q->where("school_id", $request->school_id);
        }

        return $paginate ? $q->paginate()->withQueryString() : $q->get();
    }

    public function store(array $data): Student
    {
        return $this->model->create($data);
    }

    public function update($id, array $data): bool
    {
        return $this->getById($id)->update($data);
    }

    public function getById($id): Student
    {
        return $this->model->findOrFail($id);
    }

    public function delete($id): bool
    {
        return $this->getById($id)->delete();
    }
}
