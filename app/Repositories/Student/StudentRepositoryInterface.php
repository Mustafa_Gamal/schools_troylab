<?php

namespace App\Repositories\Student;

use App\Models\Student;
use Illuminate\Http\Request;

interface StudentRepositoryInterface
{
    public function all(Request $request, $paginate = false);

    public function store(array $data): Student;

    public function update($id, array $data): bool;

    public function getById($id): Student;

    public function delete($id): bool;
}
