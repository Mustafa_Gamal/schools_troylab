<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Repositories\School\SchoolRepositoryInterface;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    private $schoolRepo;

    public function __construct(SchoolRepositoryInterface $schoolRepo)
    {
        $this->schoolRepo = $schoolRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schools = $this->schoolRepo->all($request, true);

        return view("pages.schools.index", compact("schools"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.schools.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required|max:255|min:2|string",
            "status" => "required|max:255|min:2|string"
        ]);

        School::create($data);

        return redirect()->route("schools.index")->with("status", "school created");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    // public function show(School $school)
    // {
    //     return view("pages.schools.show", compact("school"));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        return view("pages.schools.edit", compact("school"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        $data = $request->validate([
            "name" => "required|max:255|min:2|string",
            "status" => "required|max:255|min:2|string"
        ]);

        $school->update($data);

        return redirect()->route("schools.index")->with("status", "school updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        $school->delete();

        return redirect()->route("schools.index")->with("status", "school deleted");
    }
}
