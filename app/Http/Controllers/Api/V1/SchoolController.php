<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\SchoolResource;
use App\Repositories\School\SchoolRepositoryInterface;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    use ApiResponseTrait;

    private $schoolRepo;

    public function __construct(SchoolRepositoryInterface $schoolRepo)
    {
        $this->schoolRepo = $schoolRepo;
    }

    public function index(Request $request)
    {
        $schools = $this->schoolRepo->all($request);

        return $this->apiResponse(SchoolResource::collection($schools)->resolve());
    }
}
