<?php

namespace App\Http\Controllers\Api\V1;
use Illuminate\Support\Facades\Response;

trait ApiResponseTrait
{
    public function apiResponse(array $data = [], $code = 200, $message = 'success')
    {
        $data = [
            'message' => $message,
            'code' => $code,
            'data' => $data
        ];

        return Response::json($data, $code);
    }
}
