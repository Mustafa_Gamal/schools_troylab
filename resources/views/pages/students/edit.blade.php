@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Update student') }}</div>

                <div class="card-body">
                    {{ Form::model($student, ["route" => ["students.update", $student], "method" => "PUT"]) }}
                        @include('pages.students.form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
