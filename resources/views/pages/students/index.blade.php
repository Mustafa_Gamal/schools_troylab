@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">{{ __('students') }}</div>

    <div class="card-body">

        <a href="{{ route("students.create") }}" class="btn btn-primary">Create student</a>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>School</th>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($students as $student)
                <tr>
                    <td>{{ $student->id }}</td>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->school->name }}</td>
                    <td>{{ $student->order }}</td>
                    <td>{{ $student->status }}</td>
                    <td>{{ $student->created_at }}</td>
                    <td>
                        <a href="{{ route("students.edit", $student) }}" class="btn btn-info btn-sm">Edit</a>
                        {{ Form::open(["route" => ["students.destroy", $student], "method" => "DELETE", "style" =>
                        "display:inline-block"]) }}
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $students->links() }}
    </div>
</div>

@endsection
