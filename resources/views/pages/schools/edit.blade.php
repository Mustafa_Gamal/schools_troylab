@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Update School') }}</div>

                <div class="card-body">
                    {{ Form::model($school, ["route" => ["schools.update", $school], "method" => "PUT"]) }}
                        @include('pages.schools.form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
