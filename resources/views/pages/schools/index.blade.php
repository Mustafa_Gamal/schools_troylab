@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">{{ __('Schools') }}</div>

    <div class="card-body">

        <a href="{{ route("schools.create") }}" class="btn btn-primary">Create School</a>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>No students</th>
                    <th>Created at</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($schools as $school)
                <tr>
                    <td>{{ $school->id }}</td>
                    <td>{{ $school->name }}</td>
                    <td>{{ $school->status }}</td>
                    <td>
                        <a href="{{ route("students.index", ["school_id" => $school->id]) }}">
                            {{ $school->students->count() }}
                        </a>
                    </td>
                    <td>{{ $school->created_at }}</td>
                    <td>
                        <a href="{{ route("schools.edit", $school) }}" class="btn btn-info btn-sm">Edit</a>
                        {{ Form::open(["route" => ["schools.destroy", $school], "method" => "DELETE", "style" =>
                        "display:inline-block"]) }}
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $schools->links() }}
    </div>
</div>

@endsection
