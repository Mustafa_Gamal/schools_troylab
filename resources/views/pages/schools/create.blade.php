@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create School') }}</div>

                <div class="card-body">
                    {{ Form::open(["route" => "schools.store"]) }}
                        @include('pages.schools.form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
