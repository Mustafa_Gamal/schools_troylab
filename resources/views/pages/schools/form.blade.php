<div class="form-group mb-3">
    <label for="">Name</label>
    {{ Form::text("name", null, ["class" => "form-control"]) }}
</div>
<div class="form-group mb-3">
    <label for="">Status</label>
    {{ Form::select("status", ['active' => 'active', 'inactive' => 'inactive'] ,null, ["class" => "form-control"]) }}
</div>
<div class="form-group mb-3">
    <button class="btn btn-success" type="submit">Save</button>
</div>
